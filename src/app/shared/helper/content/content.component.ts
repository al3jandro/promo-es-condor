import { Component, OnInit } from '@angular/core';
import { DataDBService } from '../../service/data-db.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  cards: any;

  constructor(private jsoanservice: DataDBService) { }

  ngOnInit() {
    this.getCard();
  }

  getCard() {
    this.jsoanservice.getRead().subscribe(data => {
      this.cards = data;
    });
  }

}
