import { TestBed } from '@angular/core/testing';

import { DataDBService } from './data-db.service';

describe('DataDBService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataDBService = TestBed.get(DataDBService);
    expect(service).toBeTruthy();
  });
});
