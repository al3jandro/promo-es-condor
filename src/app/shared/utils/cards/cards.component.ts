import { Component, OnInit, Input } from '@angular/core';
import { DataDBService } from './../../service/data-db.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})

export class CardsComponent implements OnInit {

  @Input() id: number;
  card: any;
  constructor(
    private jsonService: DataDBService, private datePipe: DatePipe
  ) { }
  today: any;

  ngOnInit() {
    this.today = this.datePipe.transform(new Date(), 'MM/dd/yyyy');
    console.log(this.today);
    this.getCard(this.id);
  }

  getCard(id: number) {
    this.jsonService.getRead().subscribe(data => {
      this.card = data[id];
    });
  }
}
